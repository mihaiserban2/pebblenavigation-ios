#include <pebble.h>

//Set of enumerated keys with names and values. Identical to iOS app keys
enum {
    STREET_KEY = 0x0, // TUPLE_INTEGER
    ADVISOR_IMAGE_KEY = 0x01,
    DISTANCE_KEY = 0x02
};

static Window *window;
static TextLayer *street_layer;
static TextLayer *distance_layer;

static BitmapLayer *icon_layer;
static GBitmap *icon_bitmap = NULL;

static AppSync sync;
static uint8_t sync_buffer[123];

static void sync_error_callback(DictionaryResult dict_error, AppMessageResult app_message_error, void *context) {
    APP_LOG(APP_LOG_LEVEL_DEBUG, "App Message Sync Error: %d", app_message_error);
}

static void sync_tuple_changed_callback(const uint32_t key, const Tuple* new_tuple, const Tuple* old_tuple, void* context) {
    switch (key) {
        case ADVISOR_IMAGE_KEY:
            if (icon_bitmap) {
                gbitmap_destroy(icon_bitmap);
            }
            
            //icon_bitmap = gbitmap_create_with_data(new_tuple->value->data);
            
            icon_bitmap = gbitmap_create_with_resource(new_tuple->value->uint8);
//            size_t offset = new_tuple->value->data[0] * 104;
//            memcpy(icon_bitmap + offset, new_tuple->value->data + 1, new_tuple->length - 1);
            
            
            //memcpy(icon_bitmap->addr,new_tuple->value->data,sizeof(new_tuple->value->data));
            
            APP_LOG(APP_LOG_LEVEL_DEBUG, "App message icon bitmap: %d", (int)icon_bitmap);
            
            APP_LOG(APP_LOG_LEVEL_DEBUG, "App message icon bitmap size: %d", sizeof(new_tuple->value->data));
            
            bitmap_layer_set_bitmap(icon_layer, icon_bitmap);
            break;
            
        case STREET_KEY:
            // App Sync keeps new_tuple in sync_buffer, so we may use it directly
            text_layer_set_text(street_layer, new_tuple->value->cstring);
            break;
        case DISTANCE_KEY:
            // App Sync keeps new_tuple in sync_buffer, so we may use it directly
            text_layer_set_text(distance_layer, new_tuple->value->cstring);
            break;
    }
}

static void window_load(Window *window) {
    Layer *window_layer = window_get_root_layer(window);
    
    double widthImage = 60;
    
    GRect bounds = layer_get_bounds(window_layer);
    
    icon_layer = bitmap_layer_create(GRect(bounds.size.w/2 - widthImage/2, 5, widthImage, widthImage));
    layer_add_child(window_layer, bitmap_layer_get_layer(icon_layer));
    
    street_layer = text_layer_create((GRect) { .origin = { 0, 85 }, .size = { bounds.size.w, 40 } });
    text_layer_set_text_alignment(street_layer, GTextAlignmentCenter);
    text_layer_set_text_color(street_layer, GColorBlack);
    text_layer_set_background_color(street_layer, GColorClear);
    text_layer_set_font(street_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
    
    layer_add_child(window_layer, text_layer_get_layer(street_layer));
    
    distance_layer = text_layer_create((GRect) { .origin = { 0, 125 }, .size = { bounds.size.w, 20 } });
    text_layer_set_text_alignment(distance_layer, GTextAlignmentCenter);
    text_layer_set_text_color(distance_layer, GColorBlack);
    text_layer_set_background_color(distance_layer, GColorClear);
    text_layer_set_font(distance_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18));
    
    layer_add_child(window_layer, text_layer_get_layer(distance_layer));
    
    Tuplet initial_values[] = {
        TupletBytes(ADVISOR_IMAGE_KEY, NULL,0),
        TupletCString(STREET_KEY, "-"),
        TupletCString(DISTANCE_KEY, "-"),
    };
    app_sync_init(&sync, sync_buffer, sizeof(sync_buffer), initial_values, ARRAY_LENGTH(initial_values),
                  sync_tuple_changed_callback, sync_error_callback, NULL);
}

static void window_unload(Window *window) {
    
    app_sync_deinit(&sync);
    
    text_layer_destroy(street_layer);
    text_layer_destroy(distance_layer);
    
    if (icon_bitmap) {
        gbitmap_destroy(icon_bitmap);
    }
    
    bitmap_layer_destroy(icon_layer);
}

static void init(void) {
    window = window_create();
    window_set_background_color(window, GColorWhite);
    window_set_window_handlers(window, (WindowHandlers) {
        .load = window_load,
        .unload = window_unload,
    });
    const bool animated = true;
    window_stack_push(window, animated);
    
    //Register AppMessage events
    const int inbound_size = 64;
    const int outbound_size = 16;
    app_message_open(inbound_size, outbound_size);
}

static void deinit(void) {
    window_destroy(window);
}

int main(void) {
    init();
    
    APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);
    
    app_event_loop();
    deinit();
}
