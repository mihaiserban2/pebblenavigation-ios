//
//  SKBoundingBox.h
//  SKMaps
//
//  Copyright (c) 2013 Skobbler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

/** The SKBoundingBox is used to define a bounding box on the map between a bottom right and top left coordinates.
 */

@interface SKBoundingBox : NSObject

/** The top left coordinate of the bounding box.
 */
@property(nonatomic,assign) CLLocationCoordinate2D topLeftCoordinate;

/** The bottom right coordinate of the bounding box.
 */
@property(nonatomic,assign) CLLocationCoordinate2D bottomRightCoordinate;

/** A newly initialized SKBoundingBox.
 @param topLeft The top left coordinate for the bounding box.
 @param bottomRight The bottom right coordinate for the bounding box.
 */
+ (instancetype)boundingBoxWithTopLeftCoordinate:(CLLocationCoordinate2D)topLeft bottomRightCoordinate:(CLLocationCoordinate2D)bottomRight;

@end
