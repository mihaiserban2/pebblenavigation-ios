//
//  SKRouteAlternativeSettings.h
//  SKMaps
//
//  Copyright (c) 2013 Skobbler. All rights reserved.
//

#import "SKDefinitions.h"

/** The SKRouteAlternativeSettings is used to store information about a route alternative.
 */
@interface SKRouteAlternativeSettings : NSObject

/** The mode for the alternative route calculation. The default is SKRouteCarShortest.
 */
@property(nonatomic,assign) SKRouteMode routeMode;

/** If YES, realtime traffic data is taken into account when generating the route alternative. Default is NO;
 */
@property(nonatomic,assign) BOOL useLiveTraffic;

/** Indicates whether to use live traffic data for calculating the ETA for the alternative route.
 */
@property(nonatomic,assign) BOOL useLiveTrafficETA;

@end
