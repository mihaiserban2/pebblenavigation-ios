//
//  SKRouteState.h
//  SKMaps
//
//  Copyright (c) 2013 Skobbler. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SKCrossingDescriptor;
@class SKVisualAdviceColor;

@interface SKRouteState : NSObject

@property(nonatomic,assign)int      adviceID;
@property(nonatomic,assign)int      distanceToDestination;
@property(nonatomic,assign)int      timeToDestination;
@property(nonatomic,strong)NSString* currentStreetName;
@property(nonatomic,strong)NSString* nextStreetName;
@property(nonatomic,strong)NSString* secondStreetName;
@property(nonatomic,assign)int currentStreetType;
@property(nonatomic,assign)int nextStreetType;
@property(nonatomic,assign)int secondStreetType;
@property(nonatomic,strong)NSString *countryCode;
@property(nonatomic,strong)NSArray* audioFilesFiltered;
@property(nonatomic,strong)NSArray* audioFilesUnfiltered;
//Visual advices
@property(nonatomic,strong)NSString* currentVisualAdvicePath;
@property(nonatomic,assign)int currentVisualAdviceDistance;
@property(nonatomic,strong)NSString* secondaryVisualAdvicePath;
@property(nonatomic,assign)int secondaryVisualAdviceDistance;
@property(nonatomic,assign)BOOL isLastAdvice;
@property(nonatomic,assign)double currentVisualAdviceDistancePercent;
@property(nonatomic,assign)double currentSpeed;
@property(nonatomic,assign)double currentSpeedLimit;
@property(nonatomic,assign)BOOL isInTown;
@property(nonatomic,strong)SKCrossingDescriptor *firstCrossingDescriptor;
@property(nonatomic,strong)SKCrossingDescriptor *secondCrossingDescriptor;
@property(nonatomic,strong) NSString *exitNumber;

-(void)resetValues;
-(UIImage*)firstVisualAdviceImageWithColor:(SKVisualAdviceColor *)visualAdviceColor;
-(UIImage*)secondVisualAdviceImageWithColor:(SKVisualAdviceColor *)visualAdviceColor;

@end
