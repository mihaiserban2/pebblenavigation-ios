//
//  SKVisualAdviceColor.h
//  SKMaps
//
//  Copyright (c) 2013 Skobbler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>

/** The SKVisualAdviceColor is used to store the colors of a generated visual advice image
 */
@interface SKVisualAdviceColor : NSObject

/** The color of the allowed street.
 */
@property(nonatomic,strong) UIColor *allowedStreetColor;

/** The color of the forbidden street.
 */
@property(nonatomic,strong) UIColor *forbiddenStreetColor;

/** The color of the route's street.
 */
@property(nonatomic,strong) UIColor *routeStreetColor;

/** The background color of the visual advice.
 */
@property(nonatomic,strong) UIColor *backgroundColor;

/** A newly initialized SKVisualAdviceColor.
 */
+ (instancetype)visualAdviceColor;

@end
