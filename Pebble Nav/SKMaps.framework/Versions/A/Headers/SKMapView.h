//
//  SKMapView.h
//  SKMaps
//
//  Copyright (c) 2013 Skobbler. All rights reserved.
//

#import <UIKit/UIView.h>
#import "SKDefinitions.h"
#import "SKMapViewDelegate.h"
#import "SKMapSettings.h"

@class SKAnnotation, SKMapCustomPOI, SKBoundingBox, SKCalloutView, SKMapScaleView;

/** The SKMapView class is used for displaying the maps. It is the main class of the SKMaps framework and the entry point for all map related methods. SKMapView, like all UIKit objects, can only be managed from the main thread. Otherwise, it will result in an undefined behavior.
 */
@interface SKMapView : UIView

/** The delegate that must conform to SKMapViewDelegate protocol, used for observing user interactions changes in the map.
 */
@property(nonatomic,weak) id<SKMapViewDelegate> delegate;

#pragma mark - Settings

/** The SKMapSettings object that controls various UI and behavior settings of the map.
 */
@property(nonatomic,readonly,strong) SKMapSettings *settings;

/** The visible map coordinate region.
 */
@property(nonatomic,assign) SKCoordinateRegion visibleRegion;

/** Controls the map's bearing, in degrees. 0 = North, +90 = East, +180 = South, +270 = West.
 */
@property(nonatomic,assign) float bearing;

/** Allows or stops the map rendering when the map is visible. Default value is YES. When the map is not visible, rendering is automatically stopped.
 */
@property(nonatomic,assign) BOOL enabledRendering;

/** Flag to verify if the map is in navigation mode or not.
 */
@property(nonatomic,readonly) BOOL isInNavigationMode;

#pragma mark - Built-In Subviews

/** Can be used for displaying data about a map POI or annotation. Can be activated using showCalloutAtLocation:withOffset: method. For further details check the SKCalloutView class.
 */
@property(nonatomic,strong) SKCalloutView *calloutView;

/** Manages the displaying of the map scale. The scale value is updated automatically based on the visible region of the map. By default it is hidden.
 */
@property(nonatomic,readonly) SKMapScaleView *mapScaleView;

#pragma mark - Actions

/** Configures the map view with the map settings from the specified path.
 @param filePath The path to the configuration file.
 */
- (void)applySettingsFromFileAtPath:(NSString*)filePath;

/** Centers the map to the current position.
 */
- (void)centerOnCurrentPosition;

/** Animates the zooming of the map to a certain zoom level.
 @param zoom The zoom level the map will animate to.
 */
- (void)animateToZoomLevel:(float)zoom;

/** Animates the rotation of the map to a certain bearing.
 @param bearing The bearing to which the map will animate to, in degrees (in [ 0, 360 ] interval).
 */
- (void)animateToBearing:(float)bearing;

/** Sets the visible region of the map so that a provided coordinate bounding box will be visible.
 @param boundingBox The bounding box to be fitted.
 @param padding Padding percent. [0.0 - 1.0] values. 0.0 will fit the bounding box preciselly.
 */
- (void)fitBounds:(SKBoundingBox *)boundingBox withPadding:(float)padding;

#pragma mark - Conversions

/** Converts a CLLocationCoordinate2D location to a screen point of the current map view, if the location is in the current map region bounds.
 @param location The location to be converted.
 @return The converted CGPoint.
 */
- (CGPoint)pointForCoordinate:(CLLocationCoordinate2D)location;

/** Converts a CGPoint screen point to a CLLocationCoordinate2D location of the current map view.
 @param point The point to be converted.
 @return The converted location.
 */
- (CLLocationCoordinate2D)coordinateForPoint:(CGPoint)point;

#pragma mark - Annotations & Custom POIs

/** Adds an annotation to the map.
  @param annotation Model object for the annotation. For further details see SKAnnotation.
 */
- (BOOL)addAnnotation:(SKAnnotation *)annotation;

/** Adds a custom POI to the map.
 @param customPOI Model object of the custom POI. For further details see SKMapCustomPOI.
 */
- (BOOL)addCustomPOI:(SKMapCustomPOI *)customPOI;

/** Removes the annotation with the specified ID.
 @param identifier The ID of the annotation to be deleted from the map.
 */
- (void)removeAnnotationWithID:(int)identifier;

/** Removes all markers from the map.
 */
- (void)clearAllAnnotations;

#pragma mark - Callout view

/** Displays the callout view at the provided coordinate.
 @param coordinate The coordinate where the callout view's arrow should point.
 @param calloutOffset Provides a way for moving the callout view up, down, left or right. It's recommended to use when the callout view covers a part of the annotation image.
 @param shouldAnimate A boolean value which indicated that the callout should appear animated at the given coordinate.
 */
- (void)showCalloutAtLocation:(CLLocationCoordinate2D)coordinate withOffset:(CGPoint)calloutOffset animated:(BOOL)shouldAnimate;

/** Displays the callout view at the provided coordinate.
 @param annotation The coordinate where the callout view's arrow should point.
 @param calloutOffset Provides a way for moving the callout view up, down, left or right. It's recommended to use when the callout view covers a part of the annotation image.
 @param shouldAnimate A boolean value which indicated that the callout should appear animated at the given coordinate.
 */
- (void)showCalloutForAnnotation:(SKAnnotation *)annotation withOffset:(CGPoint)calloutOffset animated:(BOOL)shouldAnimate;

/** Hides the callout view.
 */
- (void)hideCallout;

#pragma mark - Class methods

/** Generates a PNG image on the disk, with the map from the provided bounding box.
 @param boundingBox The bounding box of the generated map.
 @param imagePath The path on the disk where the image should be generated.
 @param size The desired size of the image.
 */
+ (void)renderMapImageInBoundingBox:(SKBoundingBox *)boundingBox toPath:(NSString *)imagePath withSize:(CGSize)size;

@end