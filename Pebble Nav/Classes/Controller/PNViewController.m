//
//  PNViewController.m
//  Pebble Nav
//
//  Created by Mihai Costea on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import "PNViewController.h"
#import "PNCommunicator.h"
#import "UIImage+MonoChrome.h"

@interface PNViewController ()

@property (nonatomic) CLLocationCoordinate2D        endCoordinate;

@property (strong, nonatomic) SKMapView             *mapView;
@property (strong, nonatomic) SKPositionerService   *positionerService;

@property (strong, nonatomic) UIButton              *positionerButton;
@property (strong, nonatomic) UIButton              *cancelButton;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *processedImageView;

@end

@implementation PNViewController

-(void)showMessage
{
    u_int8_t lowerBound = 0;
    u_int8_t upperBound = 3;
    u_int8_t rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    
    NSString *alphabet  = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
    NSMutableString *s = [NSMutableString stringWithCapacity:4];
    for (NSUInteger i = 0U; i < 20; i++) {
        u_int32_t r = arc4random() % [alphabet length];
        unichar c = [alphabet characterAtIndex:r];
        [s appendFormat:@"%C", c];
    }
    
    NSNumber *iconKey = @(0); // This is our custom-defined key for the icon ID, which is of type uint8_t.
    NSNumber *temperatureKey = @(1); // This is our custom-defined key for the temperature string.
    NSDictionary *update1 = @{ iconKey:[NSNumber numberWithUint8:rndValue],
                               temperatureKey:@"12"};
    //    NSLog(@"%@",update1);
    __block NSString *message = @"";
    
//    NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
//    NSLog(@"%@",runLoop.currentMode);
    [[[PNCommunicator sharedManager] targetWatch] appMessagesPushUpdate:update1 onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
        message = error ? [error localizedDescription] : @"Update sent!";
        [self showMessage];
    }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addMap];
    [self addPositioner];
    [self addCancelButton];
    
//    NSOperationQueue *queue2 = [[NSOperationQueue alloc] init];
//    [queue2 addOperationWithBlock:^{
//        [[PNCommunicator sharedManager] sendTextMessage:@"1" forType:STREET_KEY];
//        [[PNCommunicator sharedManager] sendTextMessage:@"2" forType:DISTANCE_KEY];
//        [[PNCommunicator sharedManager] sendTextMessage:@"3" forType:STREET_KEY];
//        [[PNCommunicator sharedManager] sendTextMessage:@"4" forType:DISTANCE_KEY];
////        [self showMessage];
//    }];
    
//    [[PNCommunicator sharedManager] sendTextMessage:@"Muie" forType:STREET_KEY];
}

#pragma mark - SKRoutingService

- (void)routingService:(SKRoutingService *)routingService didFinishRouteCalculationWithInfo:(SKRouteInformation *)routeInformation {
    SKNavigationSettings *navSettings = [SKNavigationSettings navigationSettings];
    navSettings.navigationType = SKNavigationTypeSimulation;
    self.mapView.settings.displayMode = SKMapDisplayMode3D;
    self.positionerButton.hidden = YES;
    self.cancelButton.hidden = NO;
    [self.mapView clearAllAnnotations];
    self.mapView.calloutView.hidden = YES;
    [[SKRoutingService sharedInstance] startNavigationWithSettings:navSettings];
    
    NSArray *advices = [[SKRoutingService sharedInstance] routeAdviceListWithDistanceFormat:SKDistanceFormatMetric];
    for (SKRouteAdvice *advice in advices) {
        NSLog(@"Advice: %@", advice.visualAdviceFile);
        NSLog(@"Distance to advice: %d", advice.distanceToAdvice);
        NSLog(@"Distance to destination: %d", advice.distanceToDestination);
    }
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

- (void)routingService:(SKRoutingService *)routingService didChangeCurrentAdviceImage:(UIImage *)adviceImage withLastAdvice:(BOOL)isLastAdvice {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // UI UPDATION 2
        
        u_int8_t lowerBound = 0;
        u_int8_t upperBound = 11;
        u_int8_t rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
        
        [[PNCommunicator sharedManager] sendInt:rndValue forType:ADVISOR_IMAGE_KEY];
    });
    
}

- (void)routingService:(SKRoutingService *)routingService didChangeCurrentVisualAdviceDistance:(int)distance withFormattedDistance:(NSString *)formattedDistance {
    NSLog(@"Current advice distance: %d", distance);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // UI UPDATION 2
        [[PNCommunicator sharedManager] sendTextMessage:formattedDistance forType:DISTANCE_KEY];
    });
}

- (void)routingService:(SKRoutingService *)routingService didChangeDistanceToDestination:(int)distance withFormattedDistance:(NSString *)formattedDistance {
//    NSLog(@"Current distance to destination: %d", distance);
}

- (void)routingService:(SKRoutingService *)routingService didChangeFirstVisualAdvice:(BOOL)isFirstVisualAdviceChanged withSecondVisualAdvice:(BOOL)isSecondaryAdviceChanged lastAdvice:(BOOL)isLastAdvice routeState:(SKRouteState *)routeState {
//    NSLog(@"Did change first visual advice with secondary visual advice");
}

- (void)routingService:(SKRoutingService*)routingService didChangeNextStreetName:(NSString*)nextStreetName streetType:(SKStreetType) streetType countryCode:(NSString*)countryCode
{
    NSLog(@"%@ : street name",nextStreetName);
#warning hack
    if ([nextStreetName length]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // UI UPDATION 2
            [[PNCommunicator sharedManager] sendTextMessage:nextStreetName forType:STREET_KEY];
        });
    }
}

- (void)routingServiceDidReachDestination:(SKRoutingService *)routingService {
    [[SKRoutingService sharedInstance] stopNavigation];
    [[SKRoutingService sharedInstance] clearCurrentRoutes];
    [self.mapView clearAllAnnotations];
    self.cancelButton.hidden = YES;
    self.positionerButton.hidden = NO;
}

#pragma mark - SKMapDelegate

- (void)mapView:(SKMapView *)mapView didLongTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    SKSearchResult *searchObject = [[SKReverseGeocoderService sharedInstance] reverseGeocodeLocation:coordinate];
    
    mapView.calloutView.titleLabel.text = searchObject.name;
    mapView.calloutView.subtitleLabel.text = @"";
    mapView.calloutView.rightButton.hidden = YES;
    mapView.calloutView.delegate = self;
   // mapView.calloutView.calloutOffset = CGPointMake(0.0, 43.0);
    [mapView.calloutView.leftButton setImage:[UIImage imageNamed:@"icon_popup_navigate"] forState:UIControlStateNormal];
    
    self.endCoordinate = coordinate;
    [self showPinAtCoordinate:coordinate];
    
    [self.mapView showCalloutAtLocation:coordinate withOffset:CGPointMake(0.0, 45.0) animated:NO];
}

- (void)calloutView:(SKCalloutView *)calloutView didTapLeftButton:(UIButton *)leftButton {
    SKRouteSettings *route = [[SKRouteSettings alloc] init];
    route.startCoordinate = self.positionerService.currentCoordinate;
    route.destinationCoordinate = self.endCoordinate;
    route.shouldBeRendered = YES;
    route.requestAdvices = YES;
    route.numberOfRoutes = 1;
    route.routeMode = SKRoutePedestrian;
    
    [[SKRoutingService sharedInstance] calculateRoute:route];
}

#pragma mark - Private methods

- (void)addMap {
    self.positionerService = [[SKPositionerService alloc] init];

    self.mapView = [[SKMapView alloc] initWithFrame:self.view.frame];
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.mapView.mapScaleView.hidden = YES;
    self.mapView.delegate = self;
    [SKRoutingService sharedInstance].navigationDelegate = self;
    [SKRoutingService sharedInstance].routingDelegate = self;
    [SKRoutingService sharedInstance].mapView = self.mapView;
    
    SKAdvisorSettings *advisorSettings = [SKAdvisorSettings advisorSettings];
    advisorSettings.language = @"en_us";
    [SKRoutingService sharedInstance].advisorConfigurationSettings = advisorSettings;
    
    [self.mapView animateToZoomLevel:15.0];
    [self.mapView centerOnCurrentPosition];
    
    SKMapSettings *settings = self.mapView.settings;
    settings.followerMode = SKMapFollowerModePosition;
    settings.showCurrentPosition = YES;
    
    
    [self.view addSubview:self.mapView];
    [self.view sendSubviewToBack:self.mapView];
}

- (void)addPositioner {
    CGFloat y = self.view.bounds.size.height - 6.0 - 40.0;
    self.positionerButton = [[UIButton alloc] initWithFrame:CGRectMake(6.0, y, 40.0, 40.0)];
    [self.positionerButton setImage:[UIImage imageNamed:@"icon_searchcenter_currentpos.png"] forState:UIControlStateNormal];
    [self.positionerButton setBackgroundImage:[UIImage imageNamed:@"button_resultlist.png"] forState:UIControlStateNormal];
    [self.positionerButton setBackgroundImage:[UIImage imageNamed:@"button_resultlist.png"] forState:UIControlStateHighlighted];
    
    self.positionerButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.positionerButton addTarget:self action:@selector(positionerButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.positionerButton];
}

- (void)addCancelButton {
    CGFloat y = self.view.bounds.size.height - 6.0 - 40.0;
    self.cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(6.0, y, 40.0, 40.0)];
    [self.cancelButton setImage:[UIImage imageNamed:@"icon_close.png"] forState:UIControlStateNormal];
    [self.cancelButton setBackgroundImage:[UIImage imageNamed:@"button_resultlist.png"] forState:UIControlStateNormal];
    [self.cancelButton setBackgroundImage:[UIImage imageNamed:@"button_resultlist.png"] forState:UIControlStateHighlighted];
    
    self.cancelButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.cancelButton.hidden = YES;
    
    [self.view addSubview:self.cancelButton];
}

- (void)showPinAtCoordinate:(CLLocationCoordinate2D)coordinate {
    [self.mapView clearAllAnnotations];
    
    SKAnnotation *annotation = [SKAnnotation annotation];
    annotation.location = coordinate;
    [self.mapView addAnnotation:annotation];
}

#pragma mark - Actions

- (void)positionerButtonClicked {
    [self.mapView centerOnCurrentPosition];
    [self.mapView animateToZoomLevel:15.0];
}

- (void)cancelButtonClicked {
    [[SKRoutingService sharedInstance] stopNavigation];
    [[SKRoutingService sharedInstance] clearCurrentRoutes];
    [self.mapView clearAllAnnotations];
    self.cancelButton.hidden = YES;
    self.positionerButton.hidden = NO;
}

@end
