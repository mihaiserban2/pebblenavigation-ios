//
//  PNWaitingConnectionViewController.m
//  Pebble Nav
//
//  Created by Mihai Serban on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import "PNWaitingConnectionViewController.h"
#import "PNCommunicator.h"

@interface PNWaitingConnectionViewController ()

@end

@implementation PNWaitingConnectionViewController

#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didConnect:) name:kWatchDidConnectedNotification object:nil];
    }
    return self;
}

#pragma mark - View methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Observers

-(void)didConnect:(NSNotification*)notif
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - Other

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
