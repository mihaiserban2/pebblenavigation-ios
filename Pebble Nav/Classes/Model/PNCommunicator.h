//
//  PNCommunicator.h
//  Pebble Nav
//
//  Created by Mihai Serban on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PebbleKit/PebbleKit.h>
#import "PNMessage.h"

extern NSString *const kAPPUUID;
extern NSString *const kWatchDidConnectedNotification;
extern NSString *const kWatchDidDisconnectedNotification;

@interface PNCommunicator : NSObject

@property (nonatomic, strong) PBWatch *targetWatch;

+ (id)sharedManager;

+(BOOL)isConnected;

- (void)closeSession;

-(void)sendInt:(int)imageValue forType:(MESSAGE_TYPES)type;
-(void)sendTextMessage:(NSString*)message forType:(MESSAGE_TYPES)type;
-(void)sendImage:(UIImage*)image;

-(void)launchApp;
-(void)killApp;

//call in didFinishLaunching
-(void)didFinishLaunching;
@end
