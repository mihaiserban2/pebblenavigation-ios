//
//  PNMessage.m
//  Pebble Nav
//
//  Created by Mihai Serban on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import "PNMessage.h"
#import "KBPebbleImage.h"
#import <PebbleKit/PebbleKit.h>

#define MAX_OUTGOING_SIZE 105

@interface PNMessage ()

@property (nonatomic, assign) MESSAGE_TYPES messageType;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) UIImage *image;

@end

@implementation PNMessage

+(PNMessage*)messageWithType:(MESSAGE_TYPES)type andMessage:(NSString*)messageStr
{
    PNMessage *message = [[PNMessage alloc] init];
    message.messageType = type;
    message.message = messageStr;
    return message;
}

+(PNMessage*)messageWithImage:(UIImage*)image
{
    PNMessage *message = [[PNMessage alloc] init];
    message.messageType = ADVISOR_IMAGE_KEY;
    message.image = image;
    return message;
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSDictionary*)messageDict
{
    if (self.messageType == ADVISOR_IMAGE_KEY) {
        
//        PBBitmap *bitmap = [PBBitmap pebbleBitmapWithUIImage:[PNMessage imageWithImage:self.image scaledToWidth:16]];
//        

        NSData *bitmap = [KBPebbleImage ditheredBitmapFromImage:self.image withHeight:16 width:16];
        size_t length = [bitmap length];
        uint8_t j = 0;
        NSMutableData *outgoing = [[NSMutableData alloc] initWithCapacity:MAX_OUTGOING_SIZE];
        for(size_t i = 0; i < length; i += MAX_OUTGOING_SIZE-1) {
            [outgoing appendBytes:&j length:1];
            [outgoing appendData:[bitmap subdataWithRange:NSMakeRange(i, MIN(MAX_OUTGOING_SIZE-1, length - i))]];
            ++j;
        }
//        int16_t width = bitmap.bounds.size.w;
//        int16_t height = bitmap.bounds.size.h;
//        
//        // Calculate the number of bytes per row, one bit per pixel, padded to 4 bytes
//        int16_t rowSizePaddedWords = floor((width + 31) / 32);
//        int16_t widthBytes = rowSizePaddedWords * 4;
//        
//        int16_t flags = 1 << 12; // The version number is at bit 12.  Version is 1
//        
//        int16_t zero = 0;
//        NSMutableData *result = [NSMutableData data];
//        
//        [result appendBytes:&widthBytes length:sizeof(widthBytes)];
//        [result appendBytes:&flags length:sizeof(flags)];
//        [result appendBytes:&zero length:sizeof(zero)];
//        [result appendBytes:&zero length:sizeof(zero)];
//        
//        [result appendBytes:&width length:sizeof(width)];
//        [result appendBytes:&height length:sizeof(height)];
//        [result appendData:data];
        
        NSLog(@"Sending image data: %d bytes",outgoing.length);
        
        NSDictionary *update = @{[NSNumber numberWithInt:ADVISOR_IMAGE_KEY]:outgoing};
        return update;
    }
    else
    {
        NSDictionary *update = @{[NSNumber numberWithInt:self.messageType]:self.message};
        return update;
    }
}

@end
