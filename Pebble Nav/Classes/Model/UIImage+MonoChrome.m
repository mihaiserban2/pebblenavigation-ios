//
//  UIImage+MonoChrome.m
//  Pebble Nav
//
//  Created by Mihai Serban on 01/06/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import "UIImage+MonoChrome.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (MonoChrome)

+(UIImage *)convertOriginalImageToBWImage:(UIImage *)originalImage
{
    CGColorSpaceRef colorSapce = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(nil, originalImage.size.width * originalImage.scale,
                                                 originalImage.size.height * originalImage.scale, 8, originalImage.size.width * originalImage.scale,
                                                 colorSapce, kCGImageAlphaNone);
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, NO);
    CGContextDrawImage(context, CGRectMake(0, 0, originalImage.size.width, originalImage.size.height),
                       [originalImage CGImage]);
    
    CGImageRef bwImage = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSapce);
    
    UIImage *resultImage = [UIImage imageWithCGImage:bwImage];
    CGImageRelease(bwImage);
    
    UIGraphicsBeginImageContextWithOptions(originalImage.size, NO, originalImage.scale);
    [resultImage drawInRect:CGRectMake(0.0, 0.0, originalImage.size.width, originalImage.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
    
}
@end
