//
//  PNMessage.h
//  Pebble Nav
//
//  Created by Mihai Serban on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import <Foundation/Foundation.h>

//Set of enumerated keys with names and values. Identical to Pebble app keys
typedef NS_ENUM(NSInteger, MESSAGE_TYPES) {
    STREET_KEY = 0x0, // TUPLE_INTEGER
    ADVISOR_IMAGE_KEY = 0x01,
    DISTANCE_KEY = 0x02
};

@interface PNMessage : NSObject

+(PNMessage*)messageWithType:(MESSAGE_TYPES)type andMessage:(NSString*)messageStr;
+(PNMessage*)messageWithImage:(UIImage*)image;

-(NSDictionary*)messageDict;
@end
