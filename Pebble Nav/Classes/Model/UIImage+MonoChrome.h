//
//  UIImage+MonoChrome.h
//  Pebble Nav
//
//  Created by Mihai Serban on 01/06/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (MonoChrome)

+(UIImage *)convertOriginalImageToBWImage:(UIImage *)originalImage;

@end
