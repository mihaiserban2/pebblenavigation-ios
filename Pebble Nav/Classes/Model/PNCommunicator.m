//
//  PNCommunicator.m
//  Pebble Nav
//
//  Created by Mihai Serban on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import "PNCommunicator.h"
#import "KBPebbleMessageQueue.h"

NSString *const kAPPUUID = @"5d1d1257-8098-45bf-8b6d-576d0daa1cab";

//NSString *const kAPPUUID = @"28AF3DC7-E40D-490F-BEF2-29548C8B0600";
NSString *const kWatchDidConnectedNotification = @"kWatchDidConnectedNotification";
NSString *const kWatchDidDisconnectedNotification = @"kWatchDidDisconnectedNotification";

@interface PNCommunicator () <PBPebbleCentralDelegate>

@property (nonatomic, strong) NSMutableArray *messageQueue;
@property (nonatomic, strong) PNMessage *currentMessage;
@property (nonatomic, strong) KBPebbleMessageQueue *message_queue;
@end

@implementation PNCommunicator

#pragma mark - Singleton methods

+ (id)sharedManager {
    static PNCommunicator *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        
        uuid_t myAppUUIDbytes;
        NSUUID *myAppUUID = [[NSUUID alloc] initWithUUIDString:kAPPUUID];
        [myAppUUID getUUIDBytes:myAppUUIDbytes];
        
        [[PBPebbleCentral defaultCentral] setAppUUID:[NSData dataWithBytes:myAppUUIDbytes length:16]];
        
        self.messageQueue = [[NSMutableArray alloc] init];
        
        self.message_queue = [[KBPebbleMessageQueue alloc] init];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

#pragma mark - Private

- (void)setTargetWatch:(PBWatch*)watch {
    _targetWatch = watch;
    self.message_queue.watch = watch;
    // NOTE:
    // For demonstration purposes, we start communicating with the watch immediately upon connection,
    // because we are calling -appMessagesGetIsSupported: here, which implicitely opens the communication session.
    // Real world apps should communicate only if the user is actively using the app, because there
    // is one communication session that is shared between all 3rd party iOS apps.
    
    // Test if the Pebble's firmware supports AppMessages / Sports:
    [watch appMessagesGetIsSupported:^(PBWatch *watch, BOOL isAppMessagesSupported) {
        if (isAppMessagesSupported) {
            
            uuid_t myAppUUIDbytes;
            NSUUID *myAppUUID = [[NSUUID alloc] initWithUUIDString:kAPPUUID];
            [myAppUUID getUUIDBytes:myAppUUIDbytes];
        
            
            NSString *message = [NSString stringWithFormat:@"Yay! %@ supports AppMessages :D", [watch name]];
            [[[UIAlertView alloc] initWithTitle:@"Connected!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        } else {
            
            NSString *message = [NSString stringWithFormat:@"Blegh... %@ does NOT support AppMessages :'(", [watch name]];
            [[[UIAlertView alloc] initWithTitle:@"Connected..." message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

#pragma mark - Notifications

-(void)didFinishLaunching
{
    // We'd like to get called when Pebbles connect and disconnect, so become the delegate of PBPebbleCentral:
    [[PBPebbleCentral defaultCentral] setDelegate:self];
    
    //init the last watch
    [self setTargetWatch:[[PBPebbleCentral defaultCentral] lastConnectedWatch]];
}

#pragma mark - PBPebbleCentral

- (void)pebbleCentral:(PBPebbleCentral*)central watchDidConnect:(PBWatch*)watch isNew:(BOOL)isNew {
    [self setTargetWatch:watch];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kWatchDidConnectedNotification object:nil];
}

- (void)pebbleCentral:(PBPebbleCentral*)central watchDidDisconnect:(PBWatch*)watch {
    [[[UIAlertView alloc] initWithTitle:@"Disconnected!" message:[watch name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    if (_targetWatch == watch || [watch isEqual:_targetWatch]) {
        [self setTargetWatch:nil];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kWatchDidDisconnectedNotification object:nil];
}

#pragma mark - Private

-(void)sendAppMessage:(PNMessage*)message completionHandler:(void (^)())completionHandler
{
    NSLog(@"Sending message: %@",message.messageDict);
    
    [self.targetWatch appMessagesPushUpdate:[message messageDict] onSent:^(PBWatch *watch, NSDictionary *update, NSError *error) {
        if (!error) {
            NSLog(@"Successfully sent message.");
        }
        else {
            NSLog(@"Error sending message: %@", error);
        }
        completionHandler();
    }];
}

-(void)sendFirstMessage
{
    if (!self.currentMessage && [self.messageQueue count]) {
        self.currentMessage = [self.messageQueue firstObject];
        
        [self sendAppMessage:self.currentMessage completionHandler:^{
            [self.messageQueue removeObject:self.currentMessage];
            self.currentMessage = nil;
            
            [self sendFirstMessage];
        }];
    }
}

#pragma mark - Public

-(void)sendTextMessage:(NSString*)message forType:(MESSAGE_TYPES)type
{
    if (!_targetWatch) {
        return;
    }
    
    PNMessage *messageObj = [PNMessage messageWithType:type andMessage:message];
//    [self.messageQueue addObject:messageObj];
//    
//    [self sendFirstMessage];
    
    [self.message_queue enqueue:[messageObj messageDict]];
}

-(void)sendInt:(int)imageValue forType:(MESSAGE_TYPES)type
{
    if (!_targetWatch) {
        return;
    }
    
    NSDictionary *update = @{[NSNumber numberWithInt:ADVISOR_IMAGE_KEY]:[NSNumber numberWithInt8:imageValue]};
    [self.message_queue enqueue:update];
}

-(void)sendImage:(UIImage*)image
{
    if (!_targetWatch) {
        return;
    }
    
    PNMessage *messageObj = [PNMessage messageWithImage:image];
//    [self.messageQueue addObject:messageObj];
//    
//    [self sendFirstMessage];
    
    [self.message_queue enqueue:[messageObj messageDict]];
}

- (void)closeSession {
    if (!_targetWatch) {
        return;
    }
    [_targetWatch closeSession:^{
        NSLog(@"Session closed.");
    }];
}

-(void)launchApp {
    if (!_targetWatch) {
        return;
    }
    
    [_targetWatch sportsAppLaunch:^(PBWatch *watch, NSError *error) {
        if (error) {
            NSLog(@"Failed sending launch command.\n");
        } else {
            NSLog(@"Launch command sent.\n");
        }
    }];
}

-(void)killApp {
    if (!_targetWatch) {
        return;
    }
    
    [_targetWatch sportsAppKill:^(PBWatch *watch, NSError *error) {
        if (error) {
            NSLog(@"Failed sending kill command.\n");
        } else {
            NSLog(@"Kill command sent.\n");
        }
    }];
    
}

+(BOOL)isConnected
{
    return [[[PNCommunicator sharedManager] targetWatch] isConnected];
}

@end
