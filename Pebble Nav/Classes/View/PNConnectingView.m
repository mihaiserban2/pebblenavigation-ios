//
//  PNConnectingView.m
//  Pebble Nav
//
//  Created by Mihai Serban on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import "PNConnectingView.h"

@implementation PNConnectingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
