//
//  main.m
//  Pebble Nav
//
//  Created by Mihai Costea on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PNAppDelegate class]));
    }
}
