//
//  PNAppDelegate.h
//  Pebble Nav
//
//  Created by Mihai Costea on 31/05/14.
//  Copyright (c) 2014 techsylvania. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
